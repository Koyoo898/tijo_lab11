package pl.edu.pwsztar.domain.chess;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class KnightTest {

    private RulesOfGame knight = new RulesOfGame.Knight();

    @Tag("Knight")
    @ParameterizedTest
    @CsvSource({
            "5,5,3,4",
            "5,5,3,6",
            "5,5,4,3",
            "5,5,4,7",
            "5,5,6,3",
            "5,5,6,7",
            "5,5,7,4",
            "5,5,7,6",
    })

    void checkCorrectMoveForKnight(int xStart, int yStart, int xStop, int yStop){
        assertTrue(knight.isCorrectMove(xStart, yStart, xStop, yStop));
    }

    @Tag("Knight")
    @ParameterizedTest
    @CsvSource({
            "5,5,-1,7",
            "5,5,2,3",
            "5,5,3,8",
            "5,5,5,4",
            "5,5,5,5",
            "5,5,6,4",
            "5,5,6,8",
            "5,5,7,5",
    })

    void checkIncorrectMoveForKnight(int xStart, int yStart, int xStop, int yStop){
        assertFalse(knight.isCorrectMove(xStart, yStart, xStop, yStop));
    }
}
