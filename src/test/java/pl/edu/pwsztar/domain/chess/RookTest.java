package pl.edu.pwsztar.domain.chess;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RookTest {

    private RulesOfGame rook = new RulesOfGame.Rook();

    @Tag("Rook")
    @ParameterizedTest
    @CsvSource({
            "0,0,0,-5",
            "0,0,0,5",
            "0,0,0,8",
            "1,0,1,6",
            "1,0,5,0",
            "1,0,8,0",
            "2,3,5,3",
    })

    void checkCorrectMoveForRook(int xStart, int yStart, int xStop, int yStop){
        assertTrue(rook.isCorrectMove(xStart, yStart, xStop, yStop));
    }

    @Tag("Rook")
    @ParameterizedTest
    @CsvSource({
            "0,0,1,5",
            "0,0,1,8",
            "0,0,3,5",
            "0,0,6,6",
            "0,0,8,8",
            "5,5,4,4",
            "5,5,5,5"
    })

    void checkIncorrectMoveForRook(int xStart, int yStart, int xStop, int yStop){
        assertFalse(rook.isCorrectMove(xStart, yStart, xStop, yStop));
    }
}
