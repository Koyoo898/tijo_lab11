package pl.edu.pwsztar.domain.chess;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class QueenTest {

    private RulesOfGame queen = new RulesOfGame.Queen();

    @Tag("Queen")
    @ParameterizedTest
    @CsvSource({
            "5,5,4,4",
            "5,5,4,5",
            "5,5,4,6",
            "5,5,5,4",
            "5,5,6,4",
            "5,5,6,5",
            "5,5,6,6",
            "-1,-1,-21,-21",
            "-1,4,-3,2 ",
            " 0,0,20,20",
            " 0,1,2,-1",
    })

    void checkCorrectMoveForQueen(int xStart, int yStart, int xStop, int yStop){
        assertTrue(queen.isCorrectMove(xStart, yStart, xStop, yStop));
    }

    @Tag("Queen")
    @ParameterizedTest
    @CsvSource({
            "0,1,1,-2",
            "0,0,1,5",
            "0,0,1,8",
            "0,0,3,5",
            "4,4,3,2",
            "4,4,5,2",
            "4,4,5,6",
            "10,10,10,10"
    })

    void checkIncorrectMoveForQueen(int xStart, int yStart, int xStop, int yStop){
        assertFalse(queen.isCorrectMove(xStart, yStart, xStop, yStop));
    }


}
